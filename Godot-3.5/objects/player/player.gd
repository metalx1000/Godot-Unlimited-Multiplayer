extends KinematicBody2D
#Copyright Kris Occhipinti https://filmsbykris.com
#https://gitlab.com/metalx1000/Godot-Unlimited-Multiplayer
#Licensed under GPLv3
#https://gitlab.com/metalx1000/Godot-Unlimited-Multiplayer/-/blob/master/LICENSE

export (int) var player_id = 0
export (int) var speed = 3200
export (int) var jump_speed = -400
export (int) var gravity = 1000

onready var ray = $RayCast2D
onready var ray2 = $RayCast2D2

var velocity = Vector2.ZERO

onready var sprite=$sprite

func _physics_process(delta):
	velocity.x = 0
	get_input(delta)
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)



func set_color():
	var v = float(player_id*.2)
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", v)


func get_input(delta):
	var AXIS = Input.get_joy_axis(player_id,0)
	var DPAD_RIGHT = Input.is_joy_button_pressed(player_id,JOY_DPAD_RIGHT)
	var DPAD_LEFT = Input.is_joy_button_pressed(player_id,JOY_DPAD_LEFT)
	var left = Input.is_action_pressed("ui_left")
	var right = Input.is_action_pressed("ui_right")
	
	if AXIS > 0.5 || right || DPAD_RIGHT:
		sprite.flip_h = false
		sprite.play("walk")	
		velocity.x += speed * delta * 2
	if AXIS < -0.5 || left || DPAD_LEFT:
		sprite.flip_h = true
		sprite.play("walk")
		velocity.x -= speed * delta * 2
	
	if velocity.x < 30 && velocity.x > -30:
		sprite.play("default")
		
	if Input.is_action_just_pressed("jump")|| Input.is_joy_button_pressed(player_id,JOY_DS_B):
		if is_on_floor():
			$jump_snd.play()
			velocity.y = jump_speed
