# Godot-Unlimited-Multiplayer

Copyright Kris Occhipinti 2023-07-10

(https://filmsbykris.com)

License GPLv3

# Art Work by
Ansimuz https://www.patreon.com/posts/death-lamp-enemy-84137238 Artwork created by Luis Zuno (@ansimuz)

LICENSE: You may use these assets in personal or commercial projects. You can modify these assets to suit your needs. You can re-distribute the file. Credit no required but appreciated it.

LINKS Twitter @ansimuz Support my work at Patreon https://www.patreon.com/ansimuz Buy my stuff https://ansimuz.itch.io/ Get more Free Assetslike these at: http://www.ansimuz.com
